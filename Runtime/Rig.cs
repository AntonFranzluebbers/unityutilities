﻿using UnityEngine;

namespace unityutilities
{
	[AddComponentMenu("unityutilities/Rig")]
	public class Rig : MonoBehaviour
	{
		public Rigidbody rb;
		public Transform head;
		public Transform leftHand;
		public Transform rightHand;
	}
}